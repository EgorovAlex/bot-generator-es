FROM python:3.9

WORKDIR /backend/app
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .

EXPOSE 3224
ENTRYPOINT ["python3", "bot.py"]
