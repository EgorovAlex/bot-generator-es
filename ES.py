from tkinter.messagebox import NO
from telebot import types
from start import  create_welcome_kb  # мб конфликт
from db.queries import get_cur_kb_id, get_es_kb, insert_user_keyboard, get_altern_by_es_id, insert_user_alternat, get_options_by_kb, \
        get_top_alternatives_by_user, get_response_option_alternatives, update_user_alternative, get_cur_kb_id_by_response_option_id, \
        get_info_kb, update_user_kb, delete_user_es_kb, delete_user_alternatives, get_response_option_text
from keyboard import create_kb

TOP_COUNT = 5

def start_user_es_sytem(conn, cursor, teleg_id, keyboard_id, es_id):
    user_keyboard_id = insert_user_keyboard(conn, cursor, teleg_id, keyboard_id)[0]["user_keyboard_id"]
    alternatives = get_altern_by_es_id(conn, cursor, es_id)
    for alternat in alternatives:
        insert_user_alternat(conn, cursor, teleg_id, alternat["alternative_id"])

def init_es_system(conn, cursor, call):
    es_id = int(call.data.split(":")[1])
    tg_id = call.from_user.username

    cur_kb_id_arr = get_cur_kb_id(conn, cursor, es_id, tg_id)
    if len(cur_kb_id_arr) > 1:
        raise Exception("Пользователь находится сразу на нескольких этапах в ЭС")
    elif len(cur_kb_id_arr) == 0:
        cur_kb = get_es_kb(conn, cursor, es_id, 1)[0]
        start_user_es_sytem(conn, cursor, tg_id, cur_kb["keyboard_id"], es_id)
    else:
        cur_kb = cur_kb_id_arr[0]
    
    msg = cur_kb["question"]

    options = get_options_by_kb(conn, cursor, cur_kb["keyboard_id"])
    kb = create_kb(options, lambda opt: [opt["response"], f'answer:{opt["response_option_id"]}'])
    return kb, msg

def calc_top_alternatives(conn, cursor, cur_kb_info, next_kb, tg_id, top_count):
    top_alternatives = []
    adv_to_usr = None
    if (cur_kb_info["order_kb"] > 3) or (len(next_kb) == 0):
        top_alternatives = get_top_alternatives_by_user(conn, cursor, cur_kb_info["expert_system_id"], tg_id)
        adv_to_usr = ""
        if top_count > len(top_alternatives):
            top_count = len(top_alternatives)
        for i in range(top_count):
            variant = top_alternatives[i]["variant"]
            score = top_alternatives[i]["score"]
            adv_to_usr += f'{i + 1}. <b>{variant}</b> \t Рейтинг: {score} \n'
    return adv_to_usr
            
def handle_user_anwer(bot, call, conn, cursor):
    response_option_id = call.data.split(":")[1]
    tg_id = call.from_user.username
    response_option_alternatives = get_response_option_alternatives(conn, cursor, response_option_id)
    for resp_opt_alt in response_option_alternatives:
        update_user_alternative(conn, cursor, tg_id, resp_opt_alt["alternative_id"], resp_opt_alt["value"])
    
    cur_kb_id = get_cur_kb_id_by_response_option_id(conn, cursor, response_option_id)[0]["keyboard_id"]
    cur_kb_info = get_info_kb(conn, cursor, cur_kb_id)[0]
    next_kb = get_es_kb(conn, cursor, cur_kb_info["expert_system_id"], cur_kb_info["order_kb"] + 1)

    response_option_text = get_response_option_text(conn, cursor, response_option_id)[0]["response"]
    bot.send_message(call.message.chat.id, f'{cur_kb_info["question"]}\n — <b>{response_option_text}</b>', 
                            parse_mode='HTML', disable_web_page_preview=True)

    adv_to_usr = calc_top_alternatives(conn, cursor, cur_kb_info, next_kb, tg_id, TOP_COUNT)

    if len(next_kb) == 0:
        delete_user_es_kb(conn, cursor, cur_kb_id, tg_id)
        delete_user_alternatives(conn, cursor, cur_kb_info["expert_system_id"], tg_id)
        bot.send_message(call.message.chat.id, "Вы ответили на все вопросы Экспертной системы. \n <b>Рекомендуемые варианты:</b>", 
                            parse_mode='HTML', disable_web_page_preview=True)
        bot.send_message(call.message.chat.id, adv_to_usr, 
                    disable_web_page_preview=True, parse_mode='html', reply_markup=create_welcome_kb())
        return
    else:
        next_kb = next_kb[0]
        update_user_kb(conn, cursor, cur_kb_id, next_kb["keyboard_id"], tg_id)
        if adv_to_usr is not None:
            bot.send_message(call.message.chat.id, adv_to_usr, parse_mode='html',
                     disable_web_page_preview=True, reply_markup = types.ReplyKeyboardRemove())
        
        new_quest = next_kb["question"]
        new_res_options = get_options_by_kb(conn, cursor, next_kb["keyboard_id"])
        new_kb = create_kb(new_res_options, lambda opt: [opt["response"], f'answer:{opt["response_option_id"]}'])

        bot.send_message(call.message.chat.id, new_quest, reply_markup=new_kb)