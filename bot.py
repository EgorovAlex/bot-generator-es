from cgitb import html
from tkinter.messagebox import QUESTION
import telebot
import json
import os
import pandas as pd
import numpy as np
from telebot import types
from start import welcome_handler, create_welcome_kb
from config import get_item_from_config
from db.queries import get_es_systems, get_user
       
from ES import init_es_system, handle_user_anwer
from generator_ES import upload_alternatives, upload_es, upload_question_kb, upload_option, upload_option_alt_weight
from registration import reg_user
from db.connection import create_db_connection
from option_cl import Options
from keyboard import create_kb

TOKEN = get_item_from_config('MAIN').get(os.environ.get('BOT_ENV', 'token'), ' ')
BOT_DB = 'BOT_DB'

bot = telebot.TeleBot(TOKEN)

@bot.message_handler(commands=['start'])
def welcome(message):
    try:
        conn, cursor = create_db_connection(BOT_DB)

        tg_id = message.from_user.username
        users = get_user(conn, cursor, tg_id)
        if len(users) == 0:
            username = ""
            first_name = message.from_user.first_name
            last_name = message.from_user.last_name
            if first_name is not None and last_name is not None:
                username = first_name + ' ' + last_name
            reg_user(conn, cursor, tg_id, username)
        welcome_handler(message, bot)

    except Exception as ex:
        print(ex)
    finally:
        cursor.close()
        conn.close()

@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    try:
        print(call.data)
        conn, cursor = create_db_connection(BOT_DB)
        if "es:" in call.data:
            kb, msg = init_es_system(conn, cursor, call)
            bot.send_message(call.message.chat.id, msg, reply_markup=kb)
        elif "answer:" in call.data:
            handle_user_anwer(bot, call, conn, cursor)

    except Exception as ex:
        print(ex)
    finally:
        bot.delete_message(call.from_user.id, call.message.json['message_id'])
        cursor.close()
        conn.close()


@bot.message_handler(content_types=["text"])
def text_handler(message):
    try:
        conn, cursor = create_db_connection(BOT_DB)
        
        if message.chat.type == 'private':
            if message.text == 'Использование ЭС':
                es_systems = get_es_systems(conn, cursor)
                if len(es_systems) == 0:
                    bot.send_message(message.chat.id, "Экспертные системы не загружены")
                    return
                keyboard = create_kb(es_systems, lambda kb_data: [kb_data["title"], f'es:{kb_data["expert_system_id"]}'])
                bot.send_message(message.chat.id, "Выберите Экспертную систему", reply_markup=types.ReplyKeyboardRemove())
                bot.send_message(message.chat.id, "Актуальные Экспертные системы:", reply_markup=keyboard)

            elif message.text == 'Генерация ЭС':
                msg = bot.send_message(message.chat.id, "Загрузите файл с данными для ЭС в формате Excel",
                                                        reply_markup = types.ReplyKeyboardRemove())
                bot.register_next_step_handler(msg, handle_gen_es)
            else:
                bot.send_message(message.chat.id, "Это выходит за рамки моего функционала")
    except Exception as ex:
        print(ex)
        kb = create_welcome_kb()
        msg = bot.send_message(message.chat.id, str(ex) + " Ошибка", reply_markup=kb)
    finally:
        cursor.close()
        conn.close()

def handle_gen_es(message):
    try:
        conn, cursor = create_db_connection(BOT_DB)
        chat_id = message.chat.id
        tg_id = message.from_user.username
        file_info = bot.get_file(message.document.file_id)
        es_downloaded_file = bot.download_file(file_info.file_path)
        es_pd = pd.read_excel(es_downloaded_file)
        es_name = (message.document.file_name).split('.')[0]
        es_id = upload_es(conn, cursor, es_name, tg_id)
        alt_ids = upload_alternatives(conn, cursor, es_pd, es_id)
        
        questions = es_pd["question"].dropna().reset_index(drop=True)
        print(questions)

        resp_opts = es_pd["answer"][1:]
        order = 1
        for quest in questions:
            quest_kb_id = upload_question_kb(conn, cursor, quest, es_id, order)
            order+= 1
            cnt = 0
            cur_options = []
            print(resp_opts)
            for ind, opt in resp_opts.items():
                option = Options(ind, opt)
                cnt = cnt + 1
                if pd.isna(opt):
                    resp_opts = resp_opts[cnt:]
                    break
                else:
                    cur_options.append(option)

            for opt in cur_options:
                option_id = upload_option(conn, cursor, quest_kb_id, opt.opt_text)
                opt_weights = es_pd.iloc[opt.opt_row][2:].dropna().reset_index(drop=True)
                for i, weight in opt_weights.items():
                    res_opt_alt_id = upload_option_alt_weight(conn, cursor, option_id, alt_ids[i], weight)
                    print(f'alt_id {alt_ids[i]}, weitht {weight}')

        bot.send_message(message.chat.id, "Экспертная система успешно сгенерирована и доступна по нажатию на кнопку <b>\"Использование ЭС\"</b>",
                                    reply_markup=create_welcome_kb(), parse_mode="HTML")
    except Exception as ex:
        print(ex)
        kb = create_welcome_kb()
        msg = bot.send_message(message.chat.id, "Ошибка. Загрузите корректный файл в формате Excel!", reply_markup=kb)
    finally:    
        cursor.close()
        conn.close()

print("RUN!")
bot.infinity_polling(timeout=10, long_polling_timeout = 5)



