import pandas as pd
from db.connection import get_conn_param
import psycopg2
import psycopg2.extras


def execute_sql(s, conn, cursor):
    try:
        cursor.execute(s)
        results = cursor.fetchall()
        dict_res = []
        for res in results:
            dict_res.append(dict(res))
        conn.commit() 
        return dict_res
    except Exception as e:
        print(e)
        dict_res = None
        return dict_res



if __name__ == '__main__':
    pass
