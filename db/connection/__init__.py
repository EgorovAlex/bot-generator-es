from config import get_item_from_config
import psycopg2
import psycopg2.extras

def get_conn_param(source):
    _params = get_item_from_config(source)
    return _params

    # return f"{_params.get('type', '')}" \
    #        f"://{_params.get('user', '')}" \
    #        f":{_params.get('password', '')}" \
    #        f"@{_params.get('host', '')}" \
    #        f":{_params.get('port', '')}" \
    #        f"/{_params.get('database', '')}"


def create_db_connection(source):
    try:
        url_params = get_conn_param(source)
        conn = psycopg2.connect(host=url_params.get('host', ''),
                                user=url_params.get('user', ''),
                                password=url_params.get('password', ''),
                                database=url_params.get('database', ''))
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        return conn, cursor

    except Exception as e:
        print(e)

if __name__ == '__main__':
    pass