from db import execute_sql

BOT_DB = 'BOT_DB'

def get_es_systems(conn, cursor):
    query = f'''
            SELECT *
            FROM expert_system 
                                '''
    return execute_sql(query, conn, cursor)


def insert_es_system(conn, cursor, title, owner_teleg_id):
    query = f'''
        INSERT INTO  expert_system(title, owner_teleg_id)
        VALUES ('{title}', '{owner_teleg_id}')
        RETURNING  expert_system_id
                                '''
    return execute_sql(query, conn, cursor)

def insert_alternative(conn, cursor, alt, es_id):
    query = f'''
        INSERT INTO  alternative(variant, expert_system_id)
        VALUES ('{alt}', '{es_id}')
        RETURNING  alternative_id
                                '''
    return execute_sql(query, conn, cursor)

def insert_question_kb(conn, cursor, quest, es_id, order_kb):
    query = f'''
        INSERT INTO  keyboard(expert_system_id, question, order_kb)
        VALUES ('{es_id}', '{quest}', '{order_kb}')
        RETURNING  keyboard_id
                                '''
    return execute_sql(query, conn, cursor)


def insert_option(conn, cursor, quest_kb_id, opt):
    query = f'''
        INSERT INTO  response_option(keyboard_id, response)
        VALUES ('{quest_kb_id}', '{opt}')
        RETURNING  response_option_id
                                '''
    return execute_sql(query, conn, cursor)


def insert_option_alt_weight(conn, cursor, option_id, alt_id, weight):
    query = f'''
        INSERT INTO  response_option_alternative(response_option_id, alternative_id, value)
        VALUES ('{option_id}', '{alt_id}', '{weight}')
        RETURNING  response_option_alternative_id
                                '''
    return execute_sql(query, conn, cursor)


def get_user(conn, cursor, tg_id):
    query = f'''
            SELECT * FROM bot_user
            WHERE teleg_id='{tg_id}'
                                '''
    return execute_sql(query, conn, cursor)

def insert_user(conn, cursor, tg_id, now, username):
    query = f'''
        INSERT INTO  bot_user
        VALUES ('{tg_id}', '{now}', '{username}')
        RETURNING  *
                                '''
    return execute_sql(query, conn, cursor) 


def get_cur_kb_id(conn, cursor, es_id, tg_id):
    query = f'''
        SELECT * FROM keyboard
        INNER JOIN user_keyboard ON
        keyboard.keyboard_id = user_keyboard.keyboard_id AND teleg_id='{tg_id}' AND expert_system_id='{es_id}'
                                '''
    return execute_sql(query, conn, cursor) 

def get_es_kb(conn, cursor, es_id, order_kb):
    query = f'''
        SELECT * FROM keyboard
        where expert_system_id='{es_id}' and order_kb='{order_kb}'
                                '''
    return execute_sql(query, conn, cursor) 


def insert_user_keyboard(conn, cursor, teleg_id, keyboard_id):
    query = f'''
        INSERT INTO  user_keyboard(teleg_id, keyboard_id)
        VALUES ('{teleg_id}', '{keyboard_id}')
        RETURNING  user_keyboard_id
                                '''
    return execute_sql(query, conn, cursor) 

def get_altern_by_es_id(conn, cursor, es_id):
    query = f'''
        SELECT * FROM alternative
        where expert_system_id='{es_id}'
                                '''
    return execute_sql(query, conn, cursor) 


def insert_user_alternat(conn, cursor, teleg_id, alternative_id):
    query = f'''
        INSERT INTO  user_alternative(teleg_id, alternative_id)
        VALUES ('{teleg_id}', '{alternative_id}')
        RETURNING  user_alternative_id
                                '''
    return execute_sql(query, conn, cursor)    


def get_options_by_kb(conn, cursor, keyboard_id):
    query = f'''
        SELECT * FROM response_option
        where keyboard_id='{keyboard_id}'
                                '''
    return execute_sql(query, conn, cursor) 

def get_response_option_alternatives(conn, cursor, response_option_id):
    query = f'''
        SELECT * FROM response_option_alternative
        where response_option_id='{response_option_id}'
                                '''
    return execute_sql(query, conn, cursor)     

def update_user_alternative(conn, cursor, tg_id, alternative_id, value):
    query = f'''
        UPDATE user_alternative
        SET score = score + '{value}'
        where teleg_id='{tg_id}' AND alternative_id = '{alternative_id}'
        RETURNING *
                                '''
    return execute_sql(query, conn, cursor)      

def get_cur_kb_id_by_response_option_id(conn, cursor, response_option_id):
    query = f'''
        SELECT keyboard_id FROM response_option
        where response_option_id='{response_option_id}'
        LIMIT 1
                                '''
    return execute_sql(query, conn, cursor)     

def get_info_kb(conn, cursor, kb_id):
    query = f'''
        SELECT * FROM keyboard
        where keyboard_id='{kb_id}'
        LIMIT 1
                                '''
    return execute_sql(query, conn, cursor) 

def update_user_kb(conn, cursor, cur_kb_id, new_kb_id, tg_id):
    query = f'''
        UPDATE user_keyboard
        SET keyboard_id = '{new_kb_id}'
        where teleg_id='{tg_id}' AND keyboard_id = '{cur_kb_id}'
        RETURNING *
                                '''
    return execute_sql(query, conn, cursor) 

def get_top_alternatives_by_user(conn, cursor, expert_system_id, tg_id):
    query = f'''
        SELECT * from alternative
        INNER JOIN user_alternative
        USING(alternative_id)
        WHERE alternative.expert_system_id = '{expert_system_id}' AND user_alternative.teleg_id = '{tg_id}'
        ORDER BY score DESC
                                '''
    return execute_sql(query, conn, cursor) 

def delete_user_es_kb(conn, cursor, cur_kb_id, tg_id):
    query = f'''
        DELETE from user_keyboard
        WHERE teleg_id = '{tg_id}' AND keyboard_id = '{cur_kb_id}'
        RETURNING *
                                '''
    return execute_sql(query, conn, cursor) 

def delete_user_alternatives(conn, cursor, expert_system_id, tg_id):
    query = f'''
        DELETE FROM user_alternative
        WHERE teleg_id ='{tg_id}' AND alternative_id in 
                        (SELECT alternative_id 
                        FROM alternative
                        WHERE expert_system_id ='{expert_system_id}')
        RETURNING *
                                '''
    return execute_sql(query, conn, cursor) 

def get_response_option_text(conn, cursor, response_option_id):
    query = f'''
        SELECT * FROM response_option
        where response_option_id='{response_option_id}'
        LIMIT 1
                                '''
    return execute_sql(query, conn, cursor)