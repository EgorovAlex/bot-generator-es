from db.queries import *
import time

def upload_es(conn, cursor, file_name, owner_tg_id):
    es_id = insert_es_system(conn, cursor, file_name, owner_tg_id)
    return es_id[0]["expert_system_id"]

def upload_alternatives(conn, cursor, es_pd, es_id):
    alternatives = (es_pd.iloc[0]).dropna()
    alternatives_ids = []
    for alt in alternatives:
        alternatives_ids.append(insert_alternative(conn, cursor, alt, es_id)[0]["alternative_id"])

    return alternatives_ids

def upload_question_kb(conn, cursor, quest, es_id, order):
    quest_kb_id = insert_question_kb(conn, cursor, quest, es_id, order)
    return quest_kb_id[0]["keyboard_id"]
    
def upload_option(conn, cursor, quest_kb_id, opt):
    options = insert_option(conn, cursor, quest_kb_id, opt)
    return options[0]["response_option_id"]

def upload_option_alt_weight(conn, cursor, option_id, alt_id, weight):
    option_alt_weight = insert_option_alt_weight(conn, cursor, option_id, alt_id, weight)
    return option_alt_weight[0]["response_option_alternative_id"]