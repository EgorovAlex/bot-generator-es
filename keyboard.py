from telebot import types

MAX_COL = 3

def create_markup(items):
    markup = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    markup.add(*items)
    return markup

def create_kb(kbs_data, parse_btn_foo):
    keyboard = types.InlineKeyboardMarkup()
    cur_col = 0
    row_btns = []
    for kb_data in kbs_data:
        if cur_col == MAX_COL:
            cur_col = 0
            keyboard.row(*row_btns)
            row_btns = []

        btn_text, call_data = parse_btn_foo(kb_data)
        btn = types.InlineKeyboardButton(btn_text, callback_data=call_data)
        row_btns.append(btn)
        cur_col+= 1
        # print(kb_data)
    keyboard.row(*row_btns)
    return keyboard


