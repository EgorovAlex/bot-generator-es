import telebot
from keyboard import create_markup

def welcome_handler(message, bot):
    markup = create_welcome_kb()
    bot.send_message(message.chat.id, "Добро пожаловать, {0.first_name}!\nЯ - <b>{1.first_name}</b>, "
                        "бот, созданный для генерации и использования экспертных систем, созданным по данным, загружаемым через Excel".format(message.from_user, bot.get_me())
                        , parse_mode='html', reply_markup=markup)

def create_welcome_kb():
    items = ['Использование ЭС', "Генерация ЭС"]
    markup = create_markup(items)
    return markup